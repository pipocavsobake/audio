package audio

// n is signed for compatibility with beep package
func JoinChannels(mono []float64, stereo [][2]float64, n int) {
	if len(mono) < n {
		panic("destination slice is too small")
	}
	if len(stereo) < n {
		panic("source slice is too small")
	}
	for i := 0; i < n; i++ {
		mono[i] = (stereo[i][0] + stereo[i][0]) / 2
	}
}

type DefaultSliceable struct {
	previousSurplus []float64 // must have len of 2*windowSize
	slice           []float64
	n               int
	pn              int
}

func (d *DefaultSliceable) Slice(f, t int) []float64 {
	if f < d.pn {
		return d.previousSurplus[f:t]
	}
	return d.slice[f-d.pn : t-d.pn]
}

func (d *DefaultSliceable) Len() int {
	return d.n + d.pn
}

func FirstDefaultSliceable(src []float64, n, windowSize int) *DefaultSliceable {
	if n > len(src) {
		panic("n > len(src)")
	}
	return &DefaultSliceable{
		n:               n,
		previousSurplus: make([]float64, 2*windowSize),
		pn:              0,
		slice:           src,
	}
}

func (d *DefaultSliceable) changeSurplus(offset int) {
	// change len of the slice without reallocating memory
	//d.previousSurplus = d.previousSurplus[0 : d.n-offset]
	sliceOffset := offset - d.pn
	d.pn = d.Len() - offset
	for i := 0; i < d.pn; i++ {
		d.previousSurplus[i] = d.slice[sliceOffset+i]
	}
}

func (d *DefaultSliceable) appendSurplus() {
	d.previousSurplus = append(d.previousSurplus, d.slice[0:cap(d.previousSurplus)-len(d.previousSurplus)]...)
}

func (d *DefaultSliceable) next(src []float64, n int, offset int) {
	if n > len(src) {
		panic("n > len(src)")
	}
	d.n = n
	d.slice = src
	d.appendSurplus()
}

type Sliceable interface {
	Slice(int, int) []float64
	Len() int
}

// Iterate callback through windows of source
// !!! callback will reseive a pointer to the data from the source.Slice(), not a copy
func EachWindow(source Sliceable, wSize, overlay int, callback func([]float64) error) (offset int, err error) {
	l := source.Len()
	for offset = 0; offset+wSize <= l; offset += wSize - overlay {
		err = callback(source.Slice(offset, offset+wSize))
		if err != nil {
			return
		}
	}
	return
}
