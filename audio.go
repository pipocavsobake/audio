package audio

import (
	"gitlab.com/pipocavsobake/goclon/progress"

	//"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/mjibson/go-dsp/fft"

	"fmt"
	"os"
)

func pErr(err error) {
	if err != nil {
		panic(err)
	}
}

func performChunk(chunk []float64) error {
	fft.FFTReal(chunk)
	return nil
}

func performChunks(ch chan []float64, h progress.ProgressHandler) {
	i := 0
	for chunk := range ch {
		spectrum := fft.FFTReal(chunk)
		if i == 1000 || i == 2000 {
			fmt.Println(spectrum[13], spectrum[26])
		}
		h.Add(1)
		i += 1
	}
}

func Kaka() {
	filename := "/data/c2/000/000/000/000/000/000/001.mp3"
	f, err := os.Open(filename)
	pErr(err)
	s, format, err := mp3.Decode(f)
	pErr(err)
	s = s
	format = format
	if format.NumChannels != 2 {
		panic(fmt.Sprintf("%d channel(s) is (are) not implemented yet", format.NumChannels))
	}
	fmt.Println(format)
	readSize := 8 * 1024 * 1024
	windowSize := 4096
	overlay := 2048
	samplesCont := make([][2]float64, 2*readSize)
	mono := make([]float64, readSize)
	var ds *DefaultSliceable
	var offset int
	bar := &progress.Console{}
	bar.Init(s.Len() / (windowSize - overlay))
	ch := make(chan []float64, readSize/(windowSize-overlay))
	defer close(ch)
	go performChunks(ch, bar)
	for i := 0; ; i = (i + 1) % 2 {
		samples := samplesCont[i*readSize : (i+1)*readSize]
		n, ok := s.Stream(samples)
		if ok {
			//fmt.Printf("read %d picks status ok: %v\n", n, ok)
			JoinChannels(mono, samples, n)
			if ds == nil {
				ds = FirstDefaultSliceable(mono, n, windowSize)
			} else {
				ds.next(mono, n, offset)
			}
			offset, err = EachWindow(ds, windowSize, overlay, func(chunk []float64) error {
				ch <- chunk
				return nil
			})
			if err != nil {
				bar.Fail(err.Error())
				break
			}
			ds.changeSurplus(offset)
		} else {
			fmt.Println("end of file")
			bar.Finish()
			break
		}
	}
}
